package in.gore.simulation;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class Response {
	 
    private String version = "HTTP/1.1";
    private int responseCode = 200;
    private String responseReason = "OK";
    private Map<String, String> headers = new LinkedHashMap<String, String>();
    private byte[] content;
    private SocketChannel channel;
    
	private Charset charset = Charset.forName("utf-8");
	private CharsetEncoder encoder = charset.newEncoder();

    
    public Response(SocketChannel channel) {
    	this.channel = channel;
    	addDefaultHeaders();
    }

    private void addDefaultHeaders() {
        headers.put("Date", new Date().toString());
        headers.put("Server", "SimulationServer");
        headers.put("Connection", "close");
     }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponseReason() {
        return responseReason;
    }

    public String getHeader(String header) {
        return headers.get(header);
    }

    public byte[] getContent() {
        return content;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public void setResponseReason(String responseReason) {
        this.responseReason = responseReason;
    }

    public void setContent(byte[] content) {
        this.content = content;
        headers.put("Content-Length", Integer.toString(content.length));
    }

    public void setHeader(String key, String value) {
        headers.put(key, value);
    }
    
    private void writeLine(String line) throws IOException {
        channel.write(encoder.encode(CharBuffer.wrap(line + "\r\n")));
    }

    public void sendResponse() {
        try {
        	System.out.println("Sending Response");
            writeLine(version + " " + responseCode + " " + responseReason);
            for (Map.Entry<String, String> header : headers.entrySet()) {
                writeLine(header.getKey() + ": " + header.getValue());
            }
            writeLine("");
            channel.write(ByteBuffer.wrap(content));
        } catch (IOException ex) {
        	System.out.println(ex.toString());
        }
    }

}
