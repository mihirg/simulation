package in.gore.simulation;

import java.util.Date;

public class Job {
	private long timeout;
	private int connid;
	private long startTime;
	private Thread jobThread;
	public Job(long timeout, int connid, long startTime, Thread jobThread) {
		this.timeout = timeout;
		this.connid = connid;
		this.startTime = startTime;
		this.jobThread = jobThread;
	}
	
	public synchronized void cancel() {
		jobThread.interrupt();
	}
	
	public long getTimeLeft() {
		return (startTime + timeout - new Date().getTime());
	}

	public int getConnid() {
		return connid;
	}

}
