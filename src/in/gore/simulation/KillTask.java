package in.gore.simulation;

import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public class KillTask extends AbstractTask {

	public KillTask(SelectionKey key, Request request) {
		super(key, request);
	}

	@Override
	public void executeRequest() {
		
		// TODO: return 400 if the connid is invalid or absent
		String params = request.getPostData().get(0);
		String[] paramList = params.split("=");
		int connid = Integer.parseInt(paramList[1]);
		JobList.getInstance().cancelJob(connid);
		SocketChannel channel = (SocketChannel)key.channel();
		Response resp = new Response(channel);
		resp.setContent("Success".getBytes());
		resp.sendResponse();		
	}

}
