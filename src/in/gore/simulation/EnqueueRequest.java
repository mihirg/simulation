package in.gore.simulation;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.channels.SelectionKey;
import java.util.Date;

import org.json.simple.JSONObject;

public class EnqueueRequest extends  AbstractTask {

	public EnqueueRequest(SelectionKey key, Request request) {
		super(key, request);
	}


	@Override
	public void executeRequest() {
		try {
			System.out.println("Processing Request");
			// TODO: Error and parameter checking
			long timeout = 0;
			int connid = 0;
			String params = request.getRequestParams();
			String[] paramList = params.split("&");
			for (int i = 0; i < paramList.length; i++) {
				String aparam = paramList[i];
				String[] nvpair = aparam.split("=");
				if (nvpair.length != 2)
					continue;
				else {
					String name = nvpair[0];
					String value = nvpair[1];
					if (name.equals("timeout"))
						timeout = Long.parseLong(value);
					else if (name.equals("connid"))
						connid = Integer.parseInt(value);						
				}
					
			}
			
			Job j = new Job(timeout*1000, connid, new Date().getTime(), Thread.currentThread());
			// only when the job is added and the thread sleeps that the kill requests will be honored.
			JobList.getInstance().addJob(j);
			Thread.sleep(timeout * 1000);
			// At this point, since the timeout is completed, the kill request is not honored.
			// point is we can get a kill request at this point, I am choosing to ignore the same
			// since the timeout has occurred.
			JobList.getInstance().removeJob(j);
			Response resp = new Response(channel);
			resp.setHeader("Content-Type", "application/json");
			JSONObject obj = new JSONObject();
			obj.put("stat", "ok");
			StringWriter out = new StringWriter();
			obj.writeJSONString(out);
			resp.setContent(out.toString().getBytes());
			resp.sendResponse();
			System.out.println("Response Sent");
		} catch (InterruptedException exp) {
			// this if check may be un-necessary. need to read the documentation.
			// this is fired if a job is interrupted in the sleep state.
			if (exp.getMessage().equals("sleep interrupted")) {
				Response resp = new Response(channel);
				resp.setHeader("Content-Type", "application/json");
				JSONObject obj = new JSONObject();
				obj.put("stat", "killed");
				StringWriter out = new StringWriter();
				try {
					obj.writeJSONString(out);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				resp.setContent(out.toString().getBytes());
				resp.sendResponse();

				
			} else {
				exp.printStackTrace();
			}
			
		} catch (Exception exp) {
			System.out.println(exp.toString());
		} 
		
	}

}
