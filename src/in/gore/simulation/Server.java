package in.gore.simulation;

import java.io.IOException;
import java.net.InetSocketAddress;

import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
	
	private ServerSocketChannel server;
	private Selector selector;
	private ExecutorService service = Executors.newFixedThreadPool(10);
		
	public Server() {
		try {
			server = ServerSocketChannel.open();
			selector = Selector.open();
			server.socket().bind(new InetSocketAddress(8080));
			server.configureBlocking(false);
			server.register(selector, SelectionKey.OP_ACCEPT);
			
		} catch(Exception exp) {
			
		}
		
	}
	
	private void run() {
		while (true) {
			try {
				selector.selectNow();
				Iterator<SelectionKey> keys = selector.selectedKeys().iterator();
				while (keys.hasNext()) {
					SelectionKey key = keys.next();
					keys.remove();
					if (!key.isValid())
						continue;
					if (key.isAcceptable()) {
						SocketChannel client = server.accept();
						client.configureBlocking(false);
						client.register(selector, SelectionKey.OP_READ);
					} else if (key.isReadable()) {
						System.out.println("Got a readable key");
						// dont call me again
						key.interestOps(key.interestOps() & (~SelectionKey.OP_READ));
						SocketChannel channel = (SocketChannel)key.channel();
						Request re = new Request((SocketChannel)key.channel());
						AbstractTask t = null;
						if (re.getLocation().equals("/sleep"))
							t = new EnqueueRequest(key, re);
						else if (re.getLocation().equals("/server-status"))
							t = new ServerStatusTask(key, re);
						else if (re.getLocation().equals("/kill"))
							t = new KillTask(key, re);
						if (t != null)
							service.execute(t);
						else
							channel.close();
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Server s = new Server();
		s.run();

	}

}
