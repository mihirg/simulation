package in.gore.simulation;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public abstract class AbstractTask implements Runnable {
	
	protected SelectionKey key;
	protected Request request;
	protected SocketChannel channel;

	public AbstractTask(SelectionKey key, Request request) {
		this.key = key;
		this.request = request;
		this.channel = (SocketChannel)key.channel();
	}

	public abstract void executeRequest();
	
	@Override
	public void run() {
		try {
			executeRequest();
		} catch (Exception exp) {
			
		} finally {
			try {
				channel.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
