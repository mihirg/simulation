package in.gore.simulation;

import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Collection;

public class ServerStatusTask extends AbstractTask {

	public ServerStatusTask(SelectionKey key, Request request) {
		super(key, request);
	}


	@Override
	public void executeRequest() {
		
		// TODO: Send response in better formatted manner
		Collection<Job> jobList = JobList.getInstance().getAllJobs();
		StringBuffer buf = new StringBuffer();
		for (Job j : jobList) {
			buf.append(j.getTimeLeft()/1000);
			buf.append(",");
		}
		Response resp = new Response((SocketChannel)key.channel());
		resp.setContent(buf.toString().getBytes());
		resp.sendResponse();
	}

}
