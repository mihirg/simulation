package in.gore.simulation;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

public class JobList {
	
	private static JobList list = new JobList();
	
	private Map<Integer, Job> jobList = new HashMap<Integer, Job>();
	
	
	private JobList() {
		
	}
	
	public static JobList getInstance() {
		return list;
	}
	
	public synchronized void addJob(Job job) {
		if (jobList.get(job.getConnid()) == null) {
			jobList.put(job.getConnid(), job);
		}
	}
	
	public synchronized void removeJob(Job job) {
		if (jobList.containsKey(job.getConnid())) {
			jobList.remove(job.getConnid());
		}		
	}
	
	public synchronized Collection<Job> getAllJobs() {
		return jobList.values();
	}
	
	public synchronized void cancelJob(int connid) {
 		Job j = jobList.remove(connid);
 		if (j != null)
 			j.cancel();
	}
	

}
