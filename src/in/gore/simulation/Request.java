package in.gore.simulation;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class Request {
	private ByteBuffer buffer = ByteBuffer.allocateDirect(2048);
	private Charset charset = Charset.forName("utf-8");
	private CharsetDecoder decoder = charset.newDecoder();
    private String method;
    private String location;
    private String requestParams;
    private String version;
    private Map<String, String> headers = new HashMap<String, String>();
    private List<String> postData = new ArrayList<String>();


	private SocketChannel channel;
	public Request(SocketChannel channel) {
		this.channel = channel;
		initialize();
	}
	
	private void initialize() {
		try {
			int count = 0;
			StringBuffer strBuf = new StringBuffer();
			while ((count = channel.read(buffer)) > 0) {
				// make the buffer readable
				buffer.flip();
				CharBuffer cb = decoder.decode(buffer);
				strBuf.append(cb.toString());
			}
			System.out.println(strBuf.toString());
			StringTokenizer tokenizer = new StringTokenizer(strBuf.toString());
            method = tokenizer.nextToken().toUpperCase();
            String fullPath = tokenizer.nextToken();
            if (fullPath.length() > 0) {
            	String[] pathParams = fullPath.split("\\?");
            	// generally this should be two
            	for (int i = 0; i < pathParams.length; i++) {
            		if (i == 0)
            			location = pathParams[i];
            		else {
            			requestParams = pathParams[i];
            		}
            	}
            }
            version = tokenizer.nextToken();
            // parse the headers
            String[] lines = strBuf.toString().split("\r\n");
            int i = 1;
            for (i = 1; i < lines.length; i++) {
            	if (lines[i].length() > 0) {
                    String[] keyVal = lines[i].split(":", 2);
                    headers.put(keyVal[0], keyVal[1]);            		
            	} else
            		break;
            }
            i = i+1;
            for ( ; i< lines.length; i++ )
            	postData.add(lines[i]);
            
		} catch (Exception exp) {
			
		}

	}
	
	public String getMethod() {
		return method;
	}
	
	public String getLocation() {
		return location;
	}
	
	public String getVersion() {
		return version;
	}
	
	public String getRequestParams() {
		return requestParams;
	}
	
	public List<String> getPostData() {
		return postData;
	}

}
